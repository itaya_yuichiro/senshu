<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>LINEQ非公式ツール</title>
	<link rel="stylesheet" href="style.css">
	<link rel="shortcut icon" href="./favicon.ico" >
</head>
<body>
		<?php
		require_once("./phpQuery-onefile.php");
		error_reporting(0);
		header('Access-Control-Allow-Origin: *');
		$id = $_GET['id'];
		function access_mypage($cookie){
			$POST_DATA = array(
	            "login" => $_GET['id'],
	            "passwd" => $_GET['pass'],
	            "mode" => "Login",
	            "clickcheck" => 0
			);
			touch('./cookies/cookie_'.$_GET['id'].'.txt');
			$curl=curl_init();
			curl_setopt($curl,CURLOPT_POST, TRUE);
			// ↓はmultipartリクエストを許可していないサーバの場合はダメっぽいです
			// @DrunkenDad_KOBAさん、Thanks
			//curl_setopt($curl,CURLOPT_POSTFIELDS, $POST_DATA);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($POST_DATA));
			curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, FALSE);  // オレオレ証明書対策
			curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, FALSE);  // 
			curl_setopt($curl,CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($curl,CURLOPT_COOKIEJAR,      './cookies/cookie_'.$_GET['id'].'.txt');
			curl_setopt($curl,CURLOPT_FOLLOWLOCATION, TRUE); // Locationヘッダを追跡
			curl_setopt($curl, CURLOPT_URL, "https://sps.acc.senshu-u.ac.jp/ActiveCampus/module/Login.php");			
			$output= curl_exec($curl);

			$phpQueryObj = phpQuery::newDocument($output);
			$h1 = $phpQueryObj['h1'];
			if (pq($h1)->text()=="専修大学ポータル") {
				echo $h1;
			}
			

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_COOKIEFILE, './cookies/cookie_'.$_GET['id'].'.txt'); // ゲットするときはFILEだけ
			curl_setopt($curl, CURLOPT_URL, "https://sps.acc.senshu-u.ac.jp/ActiveCampus/module/MyPage.php");
			$page2 = curl_exec($curl);
//			print $page2;
			curl_close($curl);

			$phpQueryObj = phpQuery::newDocument($page2);
			// h1タグを片っ端から取得
			$a_Arr = $phpQueryObj['.bb a'];
			$boxs =  $phpQueryObj['.new_ptbox'];
			// $imgArr = $phpQueryObj['.header_photo img'];
			// $statArr = $phpQueryObj['.header_stat'];
			?>
			<ul class="list-group">
			<?php 
			$cnt = 0;
			foreach($boxs as $box) {
				?>
				<?php if ($cnt < 3){ ?>	
				<h3><?php echo pq($box)->find('.center')->text(); ?></h3>
					<?php
					$min_cnt = 0;
					foreach(pq($box)->find('tr') as $val) {
						if ($min_cnt%2==1) {
					 ?>

					  <li class="list-group-item">
					  	<p><?php echo pq($val)->find('.nw')->text() ?></p>
					  	<a href="<?php echo str_replace("..", "https://sps.acc.senshu-u.ac.jp/ActiveCampus", pq($val)->find('.bb a')->attr('href')); ?>" title=""><?php echo pq($val)->find('.bb a')->text(); ?></a><br>
					  	
					  </li>
					<?php
						}
						$min_cnt++;
				 	}
				}else if($cnt == 4){
				?>
				<h3><?php echo pq($box)->find('.center')->text(); ?></h3>
				<div class="example1">
					
				<?php  
					$path_replace = str_replace("..", "https://sps.acc.senshu-u.ac.jp/ActiveCampus", pq($box)->find('.acac')->htmlOuter()); 
					$add_class = str_replace("<table ","<table class='table' ",$path_replace);
					echo $add_class;
				?>
				</div>

				<?php					
				}
			 	$cnt++;
			}
			 ?>
			</ul>
			<?php

			// $opts = array(
			// 	'http'=>array(
			// 			'method' => 'GET',
			// 			'header' => "Accept-language: en\r\n"
			// 			. "Referer: https://sps.acc.senshu-u.ac.jp/ActiveCampus/index_after.html\r\n"
			// 			. "Cookie: ".$cookie
			// 		)
			// );
			// $context = stream_context_create($opts);
			// $response = file_get_contents('https://sps.acc.senshu-u.ac.jp/ActiveCampus/module/Login.php', false, $context);
			// print_r($response);
		}
		access_mypage($cookie);

		// // HTMLをオブジェクトとして扱う
		// $phpQueryObj = phpQuery::newDocument($HTMLData);
		// // h1タグを片っ端から取得
		// $nameArr = $phpQueryObj['.header_name'];
		// $imgArr = $phpQueryObj['.header_photo img'];
		// $statArr = $phpQueryObj['.header_stat'];
		// foreach($imgArr as $val) {
		// 	echo "<img src ='".pq($val)->attr('src')."'>";
		// }

		// echo "<br>".$nameArr->text()."さん";

		//  $a = str_replace("|","<br>",$statArr->text());
		// //echo "<br>".$stat[0];
		//  echo "<br>".$a;
		?>
</body>
</html>


